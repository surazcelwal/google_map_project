function initMap() {
    var newYork = {
        lat: 40.7128,
         lng: -73.935242
      };
    map = new google.maps.Map(document.getElementById('map'), {
      center: newYork,
      zoom: 4,
      mapTypeId: 'roadmap',
      mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
    });
  }
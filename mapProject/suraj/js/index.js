function initMap() {
    var newYork = {
        lat: 27.7172,
         lng: 85.3240
      };
    map = new google.maps.Map(document.getElementById('map'), {
      center: newYork,
      zoom: 11,
      mapTypeId: 'roadmap',
      mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
    });
  }